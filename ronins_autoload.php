<?php

$src_dir = __DIR__ . '/src/*.php';

$files = glob($src_dir);

if ($files === false) {
    throw new RuntimeException("Failed to glob for function files");
}

foreach ($files as $file) {
    require_once $file;
}

unset($files);