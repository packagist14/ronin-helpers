<?php

use Illuminate\Support\Arr;

if (! function_exists('template_crypto')) {
    function template_crypto(array $fields): array {
        $name = $fields['name'];
        return compact('name');
    }
}

if (! function_exists('cast_to_type')) {
    function cast_to_type(array $data, array $keys, string $type): array {
        foreach ($keys as $key) {
            settype($data[$key], $type);
        }
        return $data;
    }
}


if (! function_exists('fallback_crypto')) {
    function fallback_crypto(array $source, array $banned = []): array {
        $target = [];
        $only_fields = ['minmov', 'pricescale', 'active', 'pp', 'p', 'h', 'l', 'pc', 'v', 'ph',];
        $int_fields = ['active', 'v', 'ph'];
        $float_fields = ['p', 'pc', 'pp', 'h', 'l'];

        foreach ($source as $redis_key => $fields) {
            ['base' => $base, 'quote' => $quote] = redis_key_to_base_and_quote($redis_key);


            if (! array_key_exists($base, $target)) {
                $target[$base] = [];
            }

            // transformers
            if ($only_fields) {
                $fields = Arr::only($fields, $only_fields);
            }
            if ($int_fields) {
                $fields = cast_to_type($fields, $int_fields, 'integer');
            }
            if ($float_fields) {
                $fields = cast_to_type($fields, $float_fields, 'float');
            }

            // check for banned assets
            $pair = $base.$quote;
            if (in_array($pair, $banned, true)) {
                $fields['active'] = 0;
            }

            $target[$base][$quote] = $fields;
        }

        return $target;
    }
}

if (! function_exists('round_to_money')) {
    function round_to_money(array $data, array $keys, int $precision = 2): array  {
        foreach ($keys as $key) {
            if (array_key_exists($key, $data)) {
                $data[$key] = round($data[$key], $precision);
            }
        }
        return $data;
    }
}

if (! function_exists('fallback_stocks')) {
    function fallback_stocks(array $source, array $banned = []): array {
        $target = [];

        $float_fields = ['p', 'pc', 'pp', 'h', 'l'];

        foreach ($source as $redis_key => $fields) {
            $api_key = redis_key_to_base_and_quote($redis_key)['base'];
            if (in_array($api_key, $banned, true)) {
                $fields['active'] = 0;
            }
            $fields = round_to_money($fields, $float_fields);
            $target[$api_key] = $fields;
        }

        return $target;
    }
}

if (! function_exists('fallback_commodities')) {
    function fallback_commodities(array $source, array $banned = []): array {
        return fallback_stocks($source, $banned);
    }
}

if (! function_exists('fallback_fake')) {
    function fallback_fake(array $source, array $banned = []): array  {
        return $source;
    }
}


