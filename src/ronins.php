<?php

use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

// don't change. using prefix needs thorough testing.
const REDIS_CMS_DB_PREFIX = '';

const REDIS_KEY_SEPARATOR = ':';

const REDIS_ACTIVE_USERS = 'users:active';
const REDIS_USER_DATA_PREFIX = 'user';

const REDIS_STREAM_CRYPTO = 'crypto';
const REDIS_STREAM_STOCKS = 'stocks';
const REDIS_STREAM_COMMO = 'commodities';
const REDIS_STREAM_SIGNALS = 'signals';

const REDIS_KEYSPACE_USER_SETTINGS = 'settings';
const REDIS_KEYSPACE_USER_HOP = 'hop'; // history, orders, positions

const REDIS_NULL_KEYSPACE = '';
const REDIS_SCAN_COUNT = 1000;


const RONINS_UNDEFINED_PRICE = -1.11;
const RONINS_UNDEFINED_VOLUME = -1;

const RONINS_WATCH_MAXLEN = 1000;
const RONINS_WATCH_PREFIX = 'sentry';
const RONINS_WATCH_SUFFIX = ':log';
const RONINS_UPDATE_PREFIX = 'update';
const RONINS_RANGE_COUNT = 10;

const STOCKS_COMMO_QUOTE = 'USD';

if (! function_exists('redis_create_user_key')) {
    function redis_create_user_key(int $user_id): string  {
        return implode(REDIS_KEY_SEPARATOR, [REDIS_USER_DATA_PREFIX, $user_id]);
    }
}

if (! function_exists('redis_create_keyspace_user_key')) {
    function redis_create_keyspace_user_key(string $keyspace, int $user_id): string  {
        return implode(REDIS_KEY_SEPARATOR, [redis_create_user_key($user_id), $keyspace]);
    }
}

if (! function_exists('redis_clear_keyspace')) {
    function redis_clear_keyspace(string $keyspace): int  {
        $keys = redis_keys_by_keyspace($keyspace);
        return Redis::unlink($keys);
    }
}

if (! function_exists('redis_create_key_for_keyspace')) {
    function redis_create_key_for_keyspace(string $keyspace, string $base, string $quote = null): string  {
        $keyspace = strtolower($keyspace);

        $key = $base;

        if ($keyspace === REDIS_STREAM_CRYPTO) {
            $key = implode(REDIS_KEY_SEPARATOR, [$key, $quote]);
        }

        // wtf: in Order stock = stocks
        if (Str::startsWith(REDIS_STREAM_STOCKS, $keyspace)) {
            $keyspace = REDIS_STREAM_STOCKS;
        }

        return implode(REDIS_KEY_SEPARATOR, [$keyspace, $key]);
    }
}

if (! function_exists('redis_create_crypto_key')) {
    function redis_create_crypto_key(string $quote): string {
        return implode(
            REDIS_KEY_SEPARATOR, [REDIS_STREAM_CRYPTO, $quote]
        );
    }
}

if (! function_exists('redis_create_stocks_key')) {
    function redis_create_stocks_key(string $quote): string {
        return implode(
            REDIS_KEY_SEPARATOR, [REDIS_STREAM_STOCKS, $quote]
        );
    }
}

if (! function_exists('redis_create_commodities_key')) {
    function redis_create_commodities_key(string $quote): string {
        return implode(
            REDIS_KEY_SEPARATOR, [REDIS_STREAM_COMMO, $quote]
        );
    }
}

if (! function_exists('redis_keyspace_pattern')) {
    function redis_keyspace_pattern(string $keyspace): string  {
        return implode(REDIS_KEY_SEPARATOR, [$keyspace, '*']);
    }
}

if (! function_exists('redis_keys_by_keyspace')) {
    function redis_keys_by_keyspace(string $keyspace): array {
        $pattern = redis_keyspace_pattern($keyspace);
        $keys = Redis::keys($pattern);
        return is_array($keys) ? $keys : [];
    }
}

if (! function_exists('redis_keys_by_raw_pattern')) {
    function redis_keys_by_raw_pattern(string $pattern): array {
        $keys = Redis::keys($pattern);
        return is_array($keys) ? $keys : [];
    }
}

if (! function_exists('redis_hgetall_for_keyspace')) {
    function redis_hgetall_for_keyspace(string $keyspace): array {
        return redis_hgetall_for_keys(redis_keys_by_keyspace($keyspace));
    }
}

if (! function_exists('redis_hgetall_for_keys')) {
    function redis_hgetall_for_keys(array $keys): array {
        $result = Redis::pipeline(function ($pipe) use ($keys) {
            foreach ($keys as $key) {
                $pipe->hgetall($key);
            }
        });
        return array_combine($keys, $result);
    }
}

if (! function_exists('redis_hgetall')) {
    function redis_hgetall(string $redis_key): array {
        return Redis::hgetall($redis_key);
    }
}

if (! function_exists('redis_hmset')) {
    function redis_hmset(string $key, array $value): int  {
        return Redis::hmset($key, $value);
    }
}

if (! function_exists('redis_xadd')) {
    function redis_xadd(string $key, array $fields, int $maxlen = RONINS_WATCH_MAXLEN, string $new_id = '*'): array  {
        try {
            $id = Redis::xadd($key, $new_id, $fields, $maxlen, true);
            $xlen = Redis::xlen($key);
            return compact('id', 'xlen');
        } catch (Exception $e) {
            return http_e($e->getMessage());
        }
    }
}

if (! function_exists('redis_xrevrange')) {
    function redis_xrevrange(string $key, int $count = RONINS_RANGE_COUNT): array {
        return Redis::xrevrange($key, '+', '-', $count);
    }
}

if (! function_exists('redis_xrange')) {
    function redis_xrange(string $key, int $count = 11): array {
        return Redis::xrange($key, '-', '+', $count);
    }
}

if (! function_exists('redis_guess_keyspace')) {
    function redis_guess_keyspace(string $symbol): string {
        foreach (redis_all_keyspaces() as $keyspace) {
            $key_pattern = "$keyspace:$symbol*";
            $keys = redis_keys_by_raw_pattern($key_pattern);
            if (! empty($keys)) {
                return $keyspace;
            }
        }
        return REDIS_NULL_KEYSPACE;
    }
}

if (! function_exists('redis_base_exists')) {
    /*
     * return empty [] if it does not exist
     */
    function redis_base_exists(string $keyspace, string $base): bool {
        $pattern = "$keyspace:$base*";
        return (!empty(redis_keys_by_raw_pattern($pattern)));
    }
}

if (! function_exists('redis_hmsetall')) {
    function redis_hmsetall(array $cache_data): int {
        $result = Redis::pipeline(function ($pipe) use ($cache_data) {
            foreach ($cache_data as $key => $value) {
                $pipe->hmset($key, $value);
            }
        });

        return in_array(false, $result, true);
    }
}

if (! function_exists('redis_key_to_base_and_quote')) {
    function redis_key_to_base_and_quote(string $redis_key): array {

        $crypto_key_sections_count = count(explode(REDIS_KEY_SEPARATOR, redis_create_crypto_key('BTC:USDT')));
        $stocks_and_commo_key_sections_count = count(explode(REDIS_KEY_SEPARATOR, redis_create_stocks_key('APPL')));

        $parts = explode(REDIS_KEY_SEPARATOR, $redis_key);
        end($parts);

        // for Crypto prefix:keyspace:base:quote
        if (count($parts) === $crypto_key_sections_count) {
            $base = prev($parts);
            $quote = next($parts);
            return compact('base', 'quote');
        }

        // for Stocks and Commodities prefix:keyspace:base
        if (count($parts) === $stocks_and_commo_key_sections_count) {
            $quote = STOCKS_COMMO_QUOTE;
            $base =  end($parts);
            return compact('base', 'quote');
        }

        throw new LogicException("Wrong Redis key: [$redis_key]");
    }
}

if (! function_exists('redis_keys_destructured')) {
    function redis_keys_destructured(array $redis_keys): array  {
        $destructured = [];
        foreach ($redis_keys as $redis_key) {
            $destructured[$redis_key] = redis_key_to_base_and_quote($redis_key);
        }
        return $destructured;
    }
}

if (! function_exists('redis_keys_for_keyspace_base')) {
    function redis_keys_for_keyspace_base(string $keyspace): array {
        return array_column(
            redis_keys_destructured(redis_keys_by_keyspace($keyspace)),
            'base'
        );
    }
}

if (! function_exists('redis_keys_to_api_symbols')) {
    function redis_keys_to_api_symbols(array $redis_keys): array {
        return array_map(static function ($redis_key) {
            return redis_key_to_base_and_quote($redis_key)['quote'];
        }, $redis_keys);
    }
}

if (! function_exists('recursive_key_exists')) {
    function recursive_key_exists(array $keys, $storage): bool {
        if (empty($storage)) {
            return false;
        }

        if (!is_array($storage) && (count($keys) === 0)) {
            return true;
        }

        if (array_key_exists($keys[0], $storage)) {
            $storage = $storage[$keys[0]];
            array_shift($keys);
            return recursive_key_exists($keys, $storage);
        }

        return recursive_key_exists($keys, []);
    }
}

if (! function_exists('redis_all_keyspaces')) {
    function redis_all_keyspaces(): array {
        return [
            REDIS_STREAM_CRYPTO, REDIS_STREAM_STOCKS, REDIS_STREAM_COMMO,
        ];
    }
}

if (! function_exists('missed_keys')) {
    function missed_keys(array $data, array $required_keys): array {
        return array_diff_key(array_flip($required_keys), $data);
    }
}

if (! function_exists('all_required_keys')) {
    function all_required_keys(array $data, array $require_keys): bool {
        return count(missed_keys($data, $require_keys)) === 0;
    }
}

if (! function_exists('is_arr_key_live')) {
    function is_arr_key_live(string $key, array $arr)  {
        if (array_key_exists($key, $arr) && $arr[$key]) {
            return $arr[$key];
        }
        return null;
    }
}

if (! function_exists('array_diff_recursive')) {
    function array_diff_recursive($array1, $array2): array {
        $result = [];
        foreach($array1 as $key => $val) {
            if(isset($array2[$key])){
                if(is_array($val) && $array2[$key]){
                    $result[$key] = array_diff_recursive($val, $array2[$key]);
                }
            } else {
                $result[$key] = $val;
            }
        }

        return $result;
    }
}

if (! function_exists('array_diff_key_recursive')) {
    function array_diff_key_recursive(array $array1, array $array2): array {
        $result = array_diff_key($array1, $array2);
        if ($result) {
            return $result;
        }

        foreach (array_keys($array1) as $key) {
            if (is_array($array1[$key]) && is_array($array2[$key])) {
                $diff = array_diff_key_recursive($array1[$key], $array2[$key]);
                if ($diff) {
                    $result[$key] = $diff;
                }
                continue;
            }
            if (is_array($array1[$key]) && !is_array($array2[$key])) {
                $result[$key] = $array2[$key];
                continue;
            }
            if (!is_array($array1[$key]) && is_array($array2[$key])) {
                $result[$key] = $array2[$key];
            }
        }

        return $result;
    }
}

if (! function_exists('redis_create_dogkey')) {
    function redis_create_dogkey(string $keyspace, string $pair): string  {
        return implode(REDIS_KEY_SEPARATOR, [RONINS_WATCH_PREFIX, $keyspace, $pair]);
    }
}

if (! function_exists('redis_create_update_key')) {
    function redis_create_update_key(string $keyspace): string {
        return implode(REDIS_KEY_SEPARATOR, [RONINS_UPDATE_PREFIX, $keyspace]);
    }
}


if (! function_exists('redis_sadd')) {
    function redis_sadd(string $set, array $values): array  {
        try {
            $inserted = Redis::sadd($set, ...$values);
            return compact('inserted');
        } catch (Exception $e) {
            return http_e($e->getMessage());
        }
    }
}

if (! function_exists('redis_sget')) {
    function redis_sget(string $key): array  {
        $next_cursor = null;
        $sget = [];

        do {
            [$next_cursor, $values] = Redis::sscan($key,  $next_cursor);
            if (! $values) {
                break;
            }
            $sget += $values;
        } while ($next_cursor > 0);

        return $sget;
    }
}

if (! function_exists('redis_set')) {
    function redis_set(string $key, string $value): array  {
        $result = Redis::set($key, $value);
        return compact('result');
    }
}

if (! function_exists('redis_get')) {
    function redis_get(string $key): ?string {
        return Redis::get($key);
    }
}

if (! function_exists('redis_unlink')) {
    function redis_unlink(string $key)  {
        return Redis::unlink($key);
    }
}


