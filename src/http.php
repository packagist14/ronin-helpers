<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

const RONINS_ERROR_KEY = 'error';
const RONINS_CODE_KEY = 'code';

if (! function_exists('http_e')) {
    function http_e(string $error, int $code = 504): array {
        return [
            RONINS_CODE_KEY => $code,
            RONINS_ERROR_KEY => $error,
        ];
    }
}

if (! function_exists('get_guzzle_response')) {
    function get_guzzle_response(string $base_url, string $method, array $params = []): array
    {
        $query = Arr::query($params);
        $full_url = Str::finish($base_url, '/') . Str::finish($method, '?') . $query;
        $httpClient = new Client();

        try {
            $origin = $httpClient->get($full_url);
        } catch (GuzzleException $e) {
            return http_e($e->getMessage(), $e->getCode());
        }

        return compact('origin');
    }
}
