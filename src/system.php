<?php

if (! function_exists('human_readable')) {
    function human_readable(int $size): string {
        if ($size === 0) {
            return "$size b";
        }
        $unit = ['b','kb','mb','gb','tb','pb'];
        return @round($size/ (1024 ** ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    }
}

if (! function_exists('format_bytes')) {
    function format_bytes(int $bytes, int $precision = 2): string {
        $units = ['B', 'KB', 'MB' , 'GB' , 'TB'];
        $bytes = max($bytes , 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow , count($units) - 1);
        $bytes /= (1 << (10 * $pow));
        return round($bytes , $precision) . " " . $units[$pow];
    }
}

if (! function_exists('memory_get_usage_human')) {
    function memory_get_usage_human(bool $real = true): string {
        return human_readable(memory_get_usage($real));
    }
}

if (! function_exists('array_keys_exist')) {
    function array_keys_exist(array $keys, array $array): bool {
        return (count(array_intersect_key(array_flip($keys), $array)) === count($keys));
    }
}


