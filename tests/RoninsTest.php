<?php

test('redis_base_exists', function (string $keyspace, string $base, bool $expected) {
    $actual = redis_base_exists($keyspace, $base);
    expect(!empty($actual))->toEqual($expected);
})->with([
    [REDIS_STREAM_CRYPTO, 'BTC', true],
    [REDIS_STREAM_CRYPTO, 'AAPL', true],
    [REDIS_STREAM_STOCKS, 'BTC', false],
    [REDIS_STREAM_STOCKS, 'ETH', false],
]);

test('redis_guess_keyspace', function (string $base, string $expected) {
    $actual = redis_guess_keyspace($base);
    expect($actual)->toEqual($expected);
})->with([
    ['BTC', REDIS_STREAM_CRYPTO],
    ['AAPL', REDIS_STREAM_STOCKS],
    ['ROOS', REDIS_NULL_KEYSPACE],
]);

test('use prefix for redis', function () {
    // check crypto keys
    $crypto_key = redis_create_crypto_key('BTC:USDT');
    ['base' => $base, 'quote' => $quote] = redis_key_to_base_and_quote($crypto_key);
    expect($base)->toEqual('BTC');
    expect($quote)->toEqual('USDT');

    // check stocks and commo
});

test('structure identical', function (array $a1, array $a2) {
    $diff = array_diff_key_recursive($a1, $a2);
    expect($diff)->toBeEmpty();
})->with([
    [
        ['k1' => 'k1', 'k2' => 'k2', 'k3' => ['k31' => 'k31']],
        ['k1' => 'k1', 'k2' => 'k2', 'k3' => ['k31' => 'k31']],
    ],
    [
        ['k1' => 'k1', 'k2' => ['k21' => ['k211' => 'k211']], 'k3' => ['k31' => 'k31']],
        ['k1' => 'k1', 'k2' => ['k21' => ['k211' => 'k211']], 'k3' => ['k31' => 'k31']],
    ],
]);

test('structure NOT identical', function (array $a1, array $a2) {
    $diff = array_diff_key_recursive($a1, $a2);
    expect($diff)->not()->toBeEmpty();
})->with([
    [
        ['k1' => 'k1', 'k4' => 'k2', 'k3' => ['k31' => 'k31']],
        ['k1' => 'k1', 'k2' => 'k2', 'k3' => ['k31' => 'k31']],
    ],
    [
        ['k1' => 'k1', 'k2' => 'k21', 'k3' => ['k31' => 'k31']],
        ['k1' => 'k1', 'k2' => ['k21' => ['k211' => 'k211']], 'k3' => ['k31' => 'k31']],
    ],
]);
